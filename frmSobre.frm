VERSION 5.00
Object = "{B81E4E80-1DB7-11D4-8ED4-00E07D815373}#1.0#0"; "mbl.ocx"
Begin VB.Form frmSobre 
   Appearance      =   0  'Flat
   BackColor       =   &H80000005&
   BorderStyle     =   0  'None
   Caption         =   "Sobre..."
   ClientHeight    =   4575
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   6495
   BeginProperty Font 
      Name            =   "Courier New"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmSobre.frx":0000
   LinkTopic       =   "Form1"
   MouseIcon       =   "frmSobre.frx":058A
   MousePointer    =   99  'Custom
   ScaleHeight     =   4575
   ScaleWidth      =   6495
   StartUpPosition =   2  'CenterScreen
   Begin MBActiveLink.ActiveLink alkEmail 
      Height          =   255
      Left            =   1800
      TabIndex        =   8
      ToolTipText     =   "telosonline@click21.com.br"
      Top             =   2520
      Width           =   2895
      _ExtentX        =   5106
      _ExtentY        =   450
      ActiveForeColor =   12582912
      ForeColor       =   12582912
      ActiveBackColor =   16777215
      BackColor       =   16777215
      Caption         =   "telosonline@click21.com.br"
      ActiveCaption   =   "telosonline@click21.com.br"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   -1  'True
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty ActiveFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   -1  'True
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Picture         =   "frmSobre.frx":06DC
      ActivePicture   =   "frmSobre.frx":06F8
      Address         =   "telosonline@click21.com.br"
      MailSubject     =   "TOL SHUT DOWN"
      MailBody        =   "Escreva sua d�vida, reclama��o ou sugest�o!"
      AddressType     =   1
      Appearance      =   0
      ToolTip         =   "telosonline@click21.com.br"
      Alignment       =   2
      AppWindowStyle  =   3
   End
   Begin MBActiveLink.ActiveLink alkSite 
      Height          =   255
      Left            =   2040
      TabIndex        =   7
      ToolTipText     =   "www.telosonline.br.ms"
      Top             =   3240
      Width           =   2415
      _ExtentX        =   4260
      _ExtentY        =   450
      ActiveForeColor =   33023
      ForeColor       =   33023
      ActiveBackColor =   16777215
      BackColor       =   16777215
      Caption         =   "www.telosonline.br.ms"
      ActiveCaption   =   "www.telosonline.br.ms"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   -1  'True
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty ActiveFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   -1  'True
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Picture         =   "frmSobre.frx":0714
      ActivePicture   =   "frmSobre.frx":0730
      Address         =   "http://www.telosonline.br.ms"
      Appearance      =   0
      ToolTip         =   "www.telosonline.br.ms"
      Alignment       =   2
      AppWindowStyle  =   3
   End
   Begin VB.Label Label6 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Qualquer d�vida ou sugest�o envie um e-mail para:"
      Height          =   210
      Left            =   675
      TabIndex        =   6
      Top             =   2280
      Width           =   5145
   End
   Begin VB.Label Label4 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Ou visite nosso portal:"
      Height          =   210
      Left            =   2040
      TabIndex        =   5
      Top             =   3000
      Width           =   2415
   End
   Begin VB.Shape Shape1 
      Height          =   4575
      Left            =   0
      Top             =   0
      Width           =   6495
   End
   Begin VB.Image imgOk 
      Height          =   375
      Left            =   2400
      MouseIcon       =   "frmSobre.frx":074C
      MousePointer    =   99  'Custom
      Picture         =   "frmSobre.frx":089E
      ToolTipText     =   "Fechar esta janela!"
      Top             =   4080
      Width           =   1575
   End
   Begin VB.Line Line1 
      X1              =   0
      X2              =   6480
      Y1              =   3960
      Y2              =   3960
   End
   Begin VB.Label Label5 
      BackStyle       =   0  'Transparent
      Caption         =   $"frmSobre.frx":0982
      Height          =   645
      Left            =   217
      TabIndex        =   4
      Top             =   1560
      Width           =   6060
   End
   Begin VB.Label Label3 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "� Copyright 2005 Telos Online. Todos os direitos reservados."
      Height          =   210
      Left            =   120
      TabIndex        =   3
      Top             =   3720
      Width           =   6300
   End
   Begin VB.Label lblVersao 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "0.0.0"
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   210
      Left            =   960
      TabIndex        =   2
      Top             =   1200
      Width           =   525
   End
   Begin VB.Label Label2 
      BackStyle       =   0  'Transparent
      Caption         =   "Vers�o:"
      Height          =   255
      Left            =   120
      TabIndex        =   1
      Top             =   1200
      Width           =   735
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Shut Down by Telos Online"
      Height          =   210
      Left            =   120
      TabIndex        =   0
      Top             =   960
      Width           =   2625
   End
   Begin VB.Image Image1 
      Height          =   645
      Left            =   120
      Picture         =   "frmSobre.frx":0A0C
      Top             =   120
      Width           =   2820
   End
End
Attribute VB_Name = "frmSobre"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Form_Load()
    lblVersao.Caption = App.Major & "." & App.Minor & "." & App.Revision
End Sub

Private Sub imgOk_Click()
    frmSobre.Hide
End Sub
