VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Begin VB.Form frmConfig 
   Appearance      =   0  'Flat
   BackColor       =   &H80000005&
   BorderStyle     =   0  'None
   Caption         =   "Configura��es..."
   ClientHeight    =   6255
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   3750
   BeginProperty Font 
      Name            =   "Courier New"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmConfig.frx":0000
   LinkTopic       =   "Form1"
   MouseIcon       =   "frmConfig.frx":058A
   MousePointer    =   99  'Custom
   ScaleHeight     =   417
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   250
   StartUpPosition =   2  'CenterScreen
   Begin VB.TextBox txtSenha2 
      Appearance      =   0  'Flat
      Height          =   255
      IMEMode         =   3  'DISABLE
      Left            =   2040
      MaxLength       =   12
      MouseIcon       =   "frmConfig.frx":06DC
      MousePointer    =   99  'Custom
      PasswordChar    =   "�"
      TabIndex        =   12
      ToolTipText     =   "Digite-a novamente"
      Top             =   5280
      Width           =   1455
   End
   Begin VB.TextBox txtSenha 
      Appearance      =   0  'Flat
      Height          =   255
      IMEMode         =   3  'DISABLE
      Left            =   2040
      MaxLength       =   12
      MouseIcon       =   "frmConfig.frx":082E
      MousePointer    =   99  'Custom
      PasswordChar    =   "�"
      TabIndex        =   9
      ToolTipText     =   "Digite uma senha"
      Top             =   4920
      Width           =   1455
   End
   Begin VB.TextBox txtArqSomAS 
      Appearance      =   0  'Flat
      Height          =   255
      Left            =   240
      MouseIcon       =   "frmConfig.frx":0980
      MousePointer    =   99  'Custom
      TabIndex        =   7
      ToolTipText     =   "Digite o local e o arquivo completo!"
      Top             =   3840
      Width           =   3255
   End
   Begin VB.CheckBox cbxDespAS 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      Caption         =   " Alerta Sonoro"
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   240
      MouseIcon       =   "frmConfig.frx":0AD2
      MousePointer    =   99  'Custom
      TabIndex        =   6
      ToolTipText     =   "Alerta Sonoro"
      Top             =   3480
      Width           =   3255
   End
   Begin VB.CheckBox cbxStartWin 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      Caption         =   " Iniciar minimizado!"
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   240
      MouseIcon       =   "frmConfig.frx":0C24
      MousePointer    =   99  'Custom
      TabIndex        =   4
      ToolTipText     =   "Iniciar junto com o Windows"
      Top             =   2760
      Width           =   3255
   End
   Begin VB.ComboBox cbxSkin 
      Height          =   330
      ItemData        =   "frmConfig.frx":0D76
      Left            =   240
      List            =   "frmConfig.frx":0DBC
      MouseIcon       =   "frmConfig.frx":0EF1
      MousePointer    =   99  'Custom
      Style           =   2  'Dropdown List
      TabIndex        =   1
      ToolTipText     =   "Escolha o Skin"
      Top             =   1080
      Width           =   3255
   End
   Begin MSComDlg.CommonDialog cdgAbreSom 
      Left            =   0
      Top             =   0
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
      DialogTitle     =   "SOM DO DESPERTADOR"
      Filter          =   "Arquivo de Som (*.wav)|*.wav|Todos os aquivos (*.*)|*.*"
      FontName        =   "Courier New"
      InitDir         =   "App.Path"
   End
   Begin VB.Shape Shape6 
      Height          =   6255
      Left            =   0
      Top             =   0
      Width           =   3750
   End
   Begin VB.Image imgCancelar 
      Height          =   375
      Left            =   1920
      MouseIcon       =   "frmConfig.frx":1043
      MousePointer    =   99  'Custom
      Picture         =   "frmConfig.frx":1195
      ToolTipText     =   "Fechar esta janela sem salvar"
      Top             =   5760
      Width           =   1575
   End
   Begin VB.Label Label6 
      BackStyle       =   0  'Transparent
      Caption         =   "Digite novamente:"
      Height          =   255
      Left            =   240
      TabIndex        =   11
      Top             =   5280
      Width           =   2055
   End
   Begin VB.Label Label5 
      BackStyle       =   0  'Transparent
      Caption         =   "Digite uma senha:"
      Height          =   255
      Left            =   240
      TabIndex        =   10
      Top             =   4920
      Width           =   1815
   End
   Begin VB.Label Label4 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      Caption         =   "Senha"
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   240
      TabIndex        =   8
      Top             =   4680
      Width           =   615
   End
   Begin VB.Shape Shape5 
      Height          =   855
      Left            =   120
      Top             =   4800
      Width           =   3495
   End
   Begin VB.Image imgTestarAS 
      Enabled         =   0   'False
      Height          =   255
      Left            =   1920
      MouseIcon       =   "frmConfig.frx":129F
      MousePointer    =   99  'Custom
      Picture         =   "frmConfig.frx":13F1
      ToolTipText     =   "Testar"
      Top             =   4200
      Width           =   1545
   End
   Begin VB.Image imgProcurarAS 
      Enabled         =   0   'False
      Height          =   255
      Left            =   240
      MouseIcon       =   "frmConfig.frx":14BB
      MousePointer    =   99  'Custom
      Picture         =   "frmConfig.frx":160D
      ToolTipText     =   "Procurar"
      Top             =   4200
      Width           =   1545
   End
   Begin VB.Label Label3 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      Caption         =   "Despertador"
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   240
      TabIndex        =   5
      Top             =   3240
      Width           =   1215
   End
   Begin VB.Shape Shape4 
      Height          =   1215
      Left            =   120
      Top             =   3360
      Width           =   3495
   End
   Begin VB.Label Label2 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      Caption         =   "Inicializa��o"
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   240
      TabIndex        =   3
      Top             =   2520
      Width           =   1455
   End
   Begin VB.Shape Shape3 
      Height          =   495
      Left            =   120
      Top             =   2640
      Width           =   3495
   End
   Begin VB.Image imgOk 
      Height          =   375
      Left            =   240
      MouseIcon       =   "frmConfig.frx":16E7
      MousePointer    =   99  'Custom
      Picture         =   "frmConfig.frx":1839
      ToolTipText     =   "Salvar configura��es"
      Top             =   5760
      Width           =   1575
   End
   Begin VB.Label lblSkinAtual 
      Alignment       =   1  'Right Justify
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H80000005&
      Caption         =   "lblSkinAtual"
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   210
      Left            =   2280
      TabIndex        =   2
      Top             =   840
      Width           =   1260
   End
   Begin VB.Shape Shape2 
      Height          =   855
      Left            =   240
      Top             =   1440
      Width           =   3270
   End
   Begin VB.Image imgSkin 
      Appearance      =   0  'Flat
      Height          =   855
      Left            =   240
      Picture         =   "frmConfig.frx":191D
      Stretch         =   -1  'True
      Top             =   1440
      Width           =   3270
   End
   Begin VB.Label Label1 
      Alignment       =   2  'Center
      BackColor       =   &H00FFFFFF&
      Caption         =   "Skin"
      Height          =   255
      Left            =   240
      TabIndex        =   0
      Top             =   840
      Width           =   495
   End
   Begin VB.Shape Shape1 
      Height          =   1455
      Left            =   120
      Top             =   960
      Width           =   3495
   End
   Begin VB.Image imgSobre 
      Height          =   645
      Left            =   120
      MouseIcon       =   "frmConfig.frx":2D8D
      MousePointer    =   99  'Custom
      Picture         =   "frmConfig.frx":2EDF
      ToolTipText     =   "Sobre..."
      Top             =   120
      Width           =   2820
   End
End
Attribute VB_Name = "frmConfig"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'==================================================
'          ATIVA O SOM
'==================================================
Private Declare Function sndPlaySound Lib "winmm.dll" Alias "sndPlaySoundA" (ByVal lpszSoundName As String, ByVal uFlags As Long) As Long
Private Declare Function mciSendString Lib "winmm.dll" Alias "mciSendStringA" (ByVal lpstrCommand As String, ByVal lpstrReturnString As String, ByVal uReturnLength As Long, ByVal hwndCallback As Long) As Long

'==================================================
'          TocaSom
'==================================================
Public Function TocaSom()
    On Error Resume Next
        If txtArqSomAS.Text = "" Then
        MsgBox "Especifique um arquivo de som.", vbOKOnly + vbCritical
    Else
        sndPlaySound txtArqSomAS.Text, 0
        i = mciSendString("open " & txtArqSomAS.Text & " type sequencer alias myfile", 0&, 0, 0)
        i = mciSendString("play myfile wait", 0&, 0, 0)
        i = mciSendString("close myfile", 0&, 0, 0)
    End If
End Function

'==================================================
'          CheckBox do Despertador
'==================================================
Private Sub cbxDespAS_Click()
    If Right(App.Path, 1) = "\" Then
        If cbxDespAS.Value = vbChecked Then
            cbxDespAS.Value = vbChecked
            txtArqSomAS.Enabled = True
            imgProcurarAS.Picture = LoadPicture(App.Path & "btn_procurar.gif")
            imgProcurarAS.Enabled = True
            imgTestarAS.Picture = LoadPicture(App.Path & "btn_testar.gif")
            imgTestarAS.Enabled = True
        Else
            cbxDespAS.Value = vbUnchecked
            txtArqSomAS.Enabled = False
            imgProcurarAS.Picture = LoadPicture(App.Path & "btn_procurar1.gif")
            imgProcurarAS.Enabled = False
            imgTestarAS.Picture = LoadPicture(App.Path & "btn_testar1.gif")
            imgTestarAS.Enabled = False
        End If
    Else
        If cbxDespAS.Value = vbChecked Then
            cbxDespAS.Value = vbChecked
            txtArqSomAS.Enabled = True
            imgProcurarAS.Picture = LoadPicture(App.Path & "\btn_procurar.gif")
            imgProcurarAS.Enabled = True
            imgTestarAS.Picture = LoadPicture(App.Path & "\btn_testar.gif")
            imgTestarAS.Enabled = True
        Else
            cbxDespAS.Value = vbUnchecked
            txtArqSomAS.Enabled = False
            imgProcurarAS.Picture = LoadPicture(App.Path & "\btn_procurar1.gif")
            imgProcurarAS.Enabled = False
            imgTestarAS.Picture = LoadPicture(App.Path & "\btn_testar1.gif")
            imgTestarAS.Enabled = False
        End If
    End If
End Sub

'==================================================
'          CheckBox do Skin
'==================================================
Private Sub cbxSkin_Click()
    strSkin = cbxSkin.ListIndex
    Select Case strSkin
        Case 0: fileSkin = "skin_default.gif"
                fileFColor = "&H000000"
        Case 1: fileSkin = "skin_dob_azul.gif"
                fileFColor = "&HFFFFFF"
        Case 2: fileSkin = "skin_dob_laranja.gif"
                fileFColor = "&H000000"
        Case 3: fileSkin = "skin_dob_preto.gif"
                fileFColor = "&HFFFFFF"
        Case 4: fileSkin = "skin_dob_roxo.gif"
                fileFColor = "&HFFFFFF"
        Case 5: fileSkin = "skin_dob_verde.gif"
                fileFColor = "&HFFFFFF"
        Case 6: fileSkin = "skin_dob_vermelho.gif"
                fileFColor = "&HFFFFFF"
        Case 7: fileSkin = "skin_grad_amarelo.gif"
                fileFColor = "&H000000"
        Case 8: fileSkin = "skin_grad_azul.gif"
                fileFColor = "&H000000"
        Case 9: fileSkin = "skin_grad_cinza.gif"
                fileFColor = "&H000000"
        Case 10: fileSkin = "skin_grad_roxo.gif"
                fileFColor = "&H000000"
        Case 11: fileSkin = "skin_grad_verde.gif"
                fileFColor = "&H000000"
        Case 12: fileSkin = "skin_grad_vermelho.gif"
                fileFColor = "&H000000"
        Case 13: fileSkin = "skin_ond_azul.gif"
                fileFColor = "&H000000"
        Case 14: fileSkin = "skin_ond_cinza.gif"
                fileFColor = "&H000000"
        Case 15: fileSkin = "skin_ond_laranja.gif"
                fileFColor = "&H000000"
        Case 16: fileSkin = "skin_ond_roxo.gif"
                fileFColor = "&H000000"
        Case 17: fileSkin = "skin_ond_verde.gif"
                fileFColor = "&H000000"
        Case 18: fileSkin = "skin_ond_vermelho.gif"
                fileFColor = "&H000000"
    End Select
    If Right(App.Path, 1) = "\" Then
        imgSkin.Picture = LoadPicture(App.Path & "peles\" & fileSkin)
        imgSkin.Stretch = False
        imgSkin.Height = 57
        imgSkin.Width = 218
    Else
        imgSkin.Picture = LoadPicture(App.Path & "\peles\" & fileSkin)
        imgSkin.Stretch = False
        imgSkin.Height = 57
        imgSkin.Width = 218
    End If
End Sub

'==================================================
'          CheckBox da Inicializa��o
'==================================================
Private Sub cbxStartWin_Click()
    If cbxStartWin.Value = vbChecked Then
        cbxStartWin.Value = vbChecked
        strStartWin = "ON"
    Else
        cbxStartWin.Value = vbUnchecked
        strStartWin = "OFF"
    End If
End Sub

'==================================================
'          Form Activate
'==================================================
Private Sub Form_Activate()
    selSkin = frmHide.lbltmpSkin.Caption
    selSkinFont = frmHide.lbltmpSkinFont.Caption
    txtSenha.Text = frmHide.lbltmpSenha.Caption
    txtSenha2.Text = frmHide.lbltmpSenha.Caption
    Select Case selSkin
        Case "skin_default.gif": lblSkinAtual.Caption = " Padr�o "
        Case "skin_dob_azul.gif": lblSkinAtual.Caption = " Dobra - Azul "
        Case "skin_dob_laranja.gif": lblSkinAtual.Caption = " Dobra - Laranja "
        Case "skin_dob_preto.gif": lblSkinAtual.Caption = " Dobra - Preto "
        Case "skin_dob_roxo.gif": lblSkinAtual.Caption = " Dobra - Roxo "
        Case "skin_dob_verde.gif": lblSkinAtual.Caption = " Dobra - Verde "
        Case "skin_dob_vermelho.gif": lblSkinAtual.Caption = " Dobra - Vermelho "
        Case "skin_grad_amarelo.gif": lblSkinAtual.Caption = " Gradiente - Amarelo "
        Case "skin_grad_azul.gif": lblSkinAtual.Caption = " Gradiente - Azul "
        Case "skin_grad_cinza.gif": lblSkinAtual.Caption = " Gradiente - Cinza "
        Case "skin_grad_roxo.gif": lblSkinAtual.Caption = " Gradiente - Roxo "
        Case "skin_grad_verde.gif": lblSkinAtual.Caption = " Gradiente - Verde "
        Case "skin_grad_vermelho.gif": lblSkinAtual.Caption = " Gradiente - Vermelho"
        Case "skin_ond_azul.gif": lblSkinAtual.Caption = " Onda - Azul "
        Case "skin_ond_cinza.gif": lblSkinAtual.Caption = " Onda - Cinza "
        Case "skin_ond_laranja.gif": lblSkinAtual.Caption = " Onda - Laranja "
        Case "skin_ond_roxo.gif": lblSkinAtual.Caption = " Onda - Roxo "
        Case "skin_ond_verde.gif": lblSkinAtual.Caption = " Onda - Verde "
        Case "skin_ond_vermelho.gif": lblSkinAtual.Caption = " Onda - Vermelho "
    End Select
    If frmHide.lbltmpStart.Caption = "ON" Then
        strStartWin = "ON"
        cbxStartWin.Value = vbChecked
    Else
        strStartWin = "OFF"
        cbxStartWin.Value = vbUnchecked
    End If
    If Right(App.Path, 1) = "\" Then
        imgSkin.Picture = LoadPicture(App.Path & "peles\" & selSkin)
        imgSkin.Stretch = False
        imgSkin.Height = 57
        imgSkin.Width = 218
        If frmHide.lbltmpDespStat.Caption = "ON" Then
            strDespStat = "ON"
            cbxDespAS.Value = vbChecked
            txtArqSomAS.Enabled = True
            txtArqSomAS.Text = frmHide.lbltmpDespArq.Caption
            imgProcurarAS.Picture = LoadPicture(App.Path & "btn_procurar.gif")
            imgProcurarAS.Enabled = True
            imgTestarAS.Picture = LoadPicture(App.Path & "btn_testar.gif")
            imgTestarAS.Enabled = True
        Else
            strDespStat = "OFF"
            cbxDespAS.Value = vbUnchecked
            txtArqSomAS.Enabled = False
            txtArqSomAS.Text = frmHide.lbltmpDespArq.Caption
            imgProcurarAS.Picture = LoadPicture(App.Path & "btn_procurar1.gif")
            imgProcurarAS.Enabled = False
            imgTestarAS.Picture = LoadPicture(App.Path & "btn_testar1.gif")
            imgTestarAS.Enabled = False
        End If
    Else
        imgSkin.Picture = LoadPicture(App.Path & "\peles\" & selSkin)
        imgSkin.Stretch = False
        imgSkin.Height = 57
        imgSkin.Width = 218
        If frmHide.lbltmpDespStat.Caption = "ON" Then
            strDespStat = "ON"
            cbxDespAS.Value = vbChecked
            txtArqSomAS.Enabled = True
            txtArqSomAS.Text = frmHide.lbltmpDespArq.Caption
            imgProcurarAS.Picture = LoadPicture(App.Path & "\btn_procurar.gif")
            imgProcurarAS.Enabled = True
            imgTestarAS.Picture = LoadPicture(App.Path & "\btn_testar.gif")
            imgTestarAS.Enabled = True
        Else
            strDespStat = "OFF"
            cbxDespAS.Value = vbUnchecked
            txtArqSomAS.Enabled = False
            txtArqSomAS.Text = frmHide.lbltmpDespArq.Caption
            imgProcurarAS.Picture = LoadPicture(App.Path & "\btn_procurar1.gif")
            imgProcurarAS.Enabled = False
            imgTestarAS.Picture = LoadPicture(App.Path & "\btn_testar1.gif")
            imgTestarAS.Enabled = False
        End If
    End If
End Sub

'==================================================
'          Form Load
'==================================================
Private Sub Form_Load()
    strSkin = ""
End Sub

'==================================================
'          Bot�o Cancelar
'==================================================
Private Sub imgCancelar_Click()
    frmConfig.Hide
End Sub

'==================================================
'          Bot�o Ok
'==================================================
Private Sub imgOk_Click()
    strSkin = cbxSkin.ListIndex
    Select Case strSkin
        Case 0: fileSkin = "skin_default.gif"
                fileFColor = "&H000000"
        Case 1: fileSkin = "skin_dob_azul.gif"
                fileFColor = "&HFFFFFF"
        Case 2: fileSkin = "skin_dob_laranja.gif"
                fileFColor = "&H000000"
        Case 3: fileSkin = "skin_dob_preto.gif"
                fileFColor = "&HFFFFFF"
        Case 4: fileSkin = "skin_dob_roxo.gif"
                fileFColor = "&HFFFFFF"
        Case 5: fileSkin = "skin_dob_verde.gif"
                fileFColor = "&HFFFFFF"
        Case 6: fileSkin = "skin_dob_vermelho.gif"
                fileFColor = "&HFFFFFF"
        Case 7: fileSkin = "skin_grad_amarelo.gif"
                fileFColor = "&H000000"
        Case 8: fileSkin = "skin_grad_azul.gif"
                fileFColor = "&H000000"
        Case 9: fileSkin = "skin_grad_cinza.gif"
                fileFColor = "&H000000"
        Case 10: fileSkin = "skin_grad_roxo.gif"
                fileFColor = "&H000000"
        Case 11: fileSkin = "skin_grad_verde.gif"
                fileFColor = "&H000000"
        Case 12: fileSkin = "skin_grad_vermelho.gif"
                fileFColor = "&H000000"
        Case 13: fileSkin = "skin_ond_azul.gif"
                fileFColor = "&H000000"
        Case 14: fileSkin = "skin_ond_cinza.gif"
                fileFColor = "&H000000"
        Case 15: fileSkin = "skin_ond_laranja.gif"
                fileFColor = "&H000000"
        Case 16: fileSkin = "skin_ond_roxo.gif"
                fileFColor = "&H000000"
        Case 17: fileSkin = "skin_ond_verde.gif"
                fileFColor = "&H000000"
        Case 18: fileSkin = "skin_ond_vermelho.gif"
                fileFColor = "&H000000"
        Case Else
                fileSkin = frmHide.lbltmpSkin.Caption
                fileFColor = frmHide.lbltmpSkinFont.Caption
    End Select
    
    If cbxDespAS.Value = vbChecked Then
        strDespStat = "ON"
        MsgBox ("Para uma melhor execu��o, recomenda-se deixar o volume alto!"), vbInformation, "��� ATEN��O ���"
    Else
        strDespStat = "OFF"
    End If
    
    If cbxStartWin.Value = vbChecked Then
        strStartWin = "ON"
    Else
        strStartWin = "OFF"
    End If
    
    If Right(App.Path, 1) = "\" Then
        cfgFile = App.Path & "config.ini"
    Else
        cfgFile = App.Path & "\config.ini"
    End If
    
    If frmHide.lbltmpSenha.Caption <> txtSenha.Text Or frmHide.lbltmpSenha.Caption <> txtSenha2.Text Then
        If txtSenha.Text = txtSenha2.Text Then
            INIClearSetting "Senha", "PWS", cfgFile
            INIWriteSetting "Senha", "PWS", txtSenha.Text, cfgFile
            frmHide.lbltmpSenha.Caption = ""
            frmHide.lbltmpSenha.Caption = txtSenha.Text
            MsgBox ("Sua Senha foi alterada com sucesso, n�o esque�a de anot�-las!"), vbInformation, "��� ATEN��O ���"
        Else
            MsgBox ("As senha n�o conferem, sua senha n�o foi alterada."), vbExclamation, "��� ATEN��O ���"
        End If
    End If
    
    INIClearSetting "Skin", "File", cfgFile
    INIClearSetting "Skin", "FColor", cfgFile
    INIWriteSetting "Skin", "File", fileSkin, cfgFile
    INIWriteSetting "Skin", "FColor", fileFColor, cfgFile
    INIClearSetting "Despertador", "Ligado", cfgFile
    INIClearSetting "Despertador", "Arquivo", cfgFile
    INIWriteSetting "Despertador", "Ligado", strDespStat, cfgFile
    INIWriteSetting "Despertador", "Arquivo", txtArqSomAS.Text, cfgFile
    INIClearSetting "Hide", "Start", cfgFile
    INIWriteSetting "Hide", "Start", strStartWin, cfgFile
    
    If Right(App.Path, 1) = "\" Then
        strSkin = App.Path & "peles\" & fileSkin
    Else
        strSkin = App.Path & "\peles\" & fileSkin
    End If
    
    frmPrincipal.lblAtual.ForeColor = fileFColor
    frmPrincipal.lblProgramada.ForeColor = fileFColor
    frmPrincipal.Picture = LoadPicture(strSkin)
    frmHide.lbltmpSkin.Caption = ""
    frmHide.lbltmpSkin.Caption = fileSkin
    frmHide.lbltmpSkinFont.Caption = ""
    frmHide.lbltmpSkinFont.Caption = fileFColor
    frmHide.lbltmpDespStat.Caption = ""
    frmHide.lbltmpDespStat.Caption = strDespStat
    frmHide.lbltmpDespArq.Caption = ""
    frmHide.lbltmpDespArq.Caption = txtArqSomAS.Text
    frmHide.lbltmpStart.Caption = ""
    frmHide.lbltmpStart.Caption = strStartWin
    frmConfig.Hide
End Sub

'==================================================
'          Bot�o Procurar
'==================================================
Private Sub imgProcurarAS_Click()
    cdgAbreSom.ShowOpen
    txtArqSomAS.Text = cdgAbreSom.FileName
End Sub

Private Sub imgSobre_Click()
    frmSobre.Show
End Sub

'==================================================
'          Bot�o Testar
'==================================================
Private Sub imgTestarAS_Click()
    TocaSom
End Sub
