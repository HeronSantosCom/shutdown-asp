unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Buttons, ExtCtrls, StdCtrls;

type
  Tfrm1 = class(TForm)
    blackground: TShape;
    btnConcluir: TSpeedButton;
    btnOpen: TSpeedButton;
    lblStatus: TLabel;
    Image1: TImage;
    Label1: TLabel;
    Label2: TLabel;
    tmpstart: TTimer;
    tmpprinc: TTimer;
    tmpadic: TTimer;
    procedure btnConcluirClick(Sender: TObject);
    procedure btnOpenClick(Sender: TObject);
    procedure tmpstartTimer(Sender: TObject);
    procedure tmpprincTimer(Sender: TObject);
    procedure tmpadicTimer(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frm1: Tfrm1;

implementation

{$R *.dfm}

procedure Tfrm1.btnConcluirClick(Sender: TObject);
begin
HALT;
end;

procedure Tfrm1.btnOpenClick(Sender: TObject);
begin
WINEXEC('SHUTDOWN.EXE', SW_SHOWNORMAL);
HALT;
end;

procedure Tfrm1.tmpstartTimer(Sender: TObject);
begin
tmpstart.Enabled := False;
lblStatus.Caption := 'Registrando Componentes Principais...';
tmpprinc.Enabled := True;
end;

procedure Tfrm1.tmpprincTimer(Sender: TObject);
begin
WINEXEC('regsvr32 /s COMDLG32.OCX', SW_SHOWNORMAL);
WINEXEC('regsvr32 /s MSCOMCTL.OCX', SW_SHOWNORMAL);
WINEXEC('regsvr32 /s MSMASK32.OCX', SW_SHOWNORMAL);
WINEXEC('regsvr32 /s asycfilt.dll', SW_SHOWNORMAL);
WINEXEC('regsvr32 /s COMCAT.DLL', SW_SHOWNORMAL);
WINEXEC('regsvr32 /s msvbvm60.dll', SW_SHOWNORMAL);
WINEXEC('regsvr32 /s oleaut32.dll', SW_SHOWNORMAL);
WINEXEC('regsvr32 /s olepro32.dll', SW_SHOWNORMAL);
WINEXEC('regsvr32 /s VB6STKIT.DLL', SW_SHOWNORMAL);
tmpprinc.Enabled := False;
lblStatus.Caption := 'Registrando Componentes Adicionais...';
tmpadic.Enabled := True;
end;

procedure Tfrm1.tmpadicTimer(Sender: TObject);
begin
WINEXEC('regsvr32 /s FlshTray.ocx', SW_SHOWNORMAL);
WINEXEC('regsvr32 /s mbl.ocx', SW_SHOWNORMAL);
WINEXEC('regsvr32 /s CTINI.DLL', SW_SHOWNORMAL);
tmpadic.Enabled := False;
lblStatus.Caption := 'Instalação Completa...';
btnConcluir.Enabled := True;
btnOpen.Enabled := True;
end;

procedure Tfrm1.FormCreate(Sender: TObject);
begin
tmpstart.Enabled := True;
end;

end.
