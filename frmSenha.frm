VERSION 5.00
Begin VB.Form frmSenha 
   Appearance      =   0  'Flat
   BackColor       =   &H80000005&
   BorderStyle     =   0  'None
   Caption         =   "Informe sua senha..."
   ClientHeight    =   2175
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   3735
   BeginProperty Font 
      Name            =   "Courier New"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmSenha.frx":0000
   LinkTopic       =   "Form1"
   MouseIcon       =   "frmSenha.frx":058A
   MousePointer    =   99  'Custom
   ScaleHeight     =   145
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   249
   StartUpPosition =   2  'CenterScreen
   Begin VB.TextBox txtSenha 
      Appearance      =   0  'Flat
      Height          =   255
      IMEMode         =   3  'DISABLE
      Left            =   1208
      MouseIcon       =   "frmSenha.frx":06DC
      MousePointer    =   99  'Custom
      PasswordChar    =   "�"
      TabIndex        =   1
      ToolTipText     =   "Digite sua senha"
      Top             =   1200
      Width           =   1335
   End
   Begin VB.Image imgOk 
      Height          =   375
      Left            =   255
      MouseIcon       =   "frmSenha.frx":082E
      MousePointer    =   99  'Custom
      Picture         =   "frmSenha.frx":0980
      ToolTipText     =   "DESATIVAR"
      Top             =   1560
      Width           =   1575
   End
   Begin VB.Image imgCancelar 
      Height          =   375
      Left            =   1935
      MouseIcon       =   "frmSenha.frx":0A64
      MousePointer    =   99  'Custom
      Picture         =   "frmSenha.frx":0BB6
      ToolTipText     =   "N�O DESATIVAR"
      Top             =   1560
      Width           =   1575
   End
   Begin VB.Shape Shape1 
      Height          =   2175
      Left            =   0
      Top             =   0
      Width           =   3735
   End
   Begin VB.Label Label1 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      Caption         =   "Informe a senha para desativar:"
      Height          =   255
      Left            =   240
      TabIndex        =   0
      Top             =   840
      Width           =   3255
   End
   Begin VB.Image imgSobre 
      Height          =   645
      Left            =   120
      MouseIcon       =   "frmSenha.frx":0CC0
      MousePointer    =   99  'Custom
      Picture         =   "frmSenha.frx":0E12
      ToolTipText     =   "Sobre..."
      Top             =   120
      Width           =   2820
   End
End
Attribute VB_Name = "frmSenha"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'==================================================
'          Form Activate
'==================================================
Private Sub Form_Activate()
txtSenha.SetFocus
End Sub

'==================================================
'          Bot�o Cancelar
'==================================================
Private Sub imgCancelar_Click()
    frmSenha.Hide
End Sub

'==================================================
'          Bot�o Ok
'==================================================
Private Sub imgOk_Click()
    If frmHide.lbltmpSenha.Caption = txtSenha.Text Then
        txtSenha.Text = ""
        NoSysIconFlash
        
        frmHide.cbxDesligar.Value = vbUnchecked
        frmHide.cbxDespertador.Value = vbUnchecked
        frmHide.cbxLogoff.Value = vbUnchecked
        'frmHide.cbxHibernar.Value = vbUnchecked
        frmHide.cbxReiniciar.Value = vbUnchecked
        
        
        If Right(App.Path, 1) = "\" Then
            frmPrincipal.imgDespertador.Picture = LoadPicture(App.Path & "despertador.gif")
            frmPrincipal.imgLogoff.Picture = LoadPicture(App.Path & "Logoff.gif")
            'frmPrincipal.imgHibernar.Picture = LoadPicture(App.Path & "Hibernar.gif")
            frmPrincipal.imgReiniciar.Picture = LoadPicture(App.Path & "Reiniciar.gif")
            frmPrincipal.imgDesligar.Picture = LoadPicture(App.Path & "Desligar.gif")
        Else
            frmPrincipal.imgDespertador.Picture = LoadPicture(App.Path & "\despertador.gif")
            frmPrincipal.imgLogoff.Picture = LoadPicture(App.Path & "\Logoff.gif")
            'frmPrincipal.imgHibernar.Picture = LoadPicture(App.Path & "\Hibernar.gif")
            frmPrincipal.imgReiniciar.Picture = LoadPicture(App.Path & "\Reiniciar.gif")
            frmPrincipal.imgDesligar.Picture = LoadPicture(App.Path & "\Desligar.gif")
        End If
        
        frmPrincipal.imgDespertador.Enabled = True
        frmPrincipal.imgLogoff.Enabled = True
        'frmPrincipal.imgHibernar.Enabled = True
        frmPrincipal.imgReiniciar.Enabled = True
        frmPrincipal.imgDesligar.Enabled = True
        
        frmPrincipal.tmrDespertador.Enabled = False
        frmPrincipal.tmrLogoff.Enabled = False
        'frmPrincipal.tmrHibernar.Enabled = False
        frmPrincipal.tmrReiniciar.Enabled = False
        frmPrincipal.tmrDesligar.Enabled = False
        
        frmPrincipal.mekProgramada.SelStart = 0
        frmPrincipal.mekProgramada.SelLength = Len(frmPrincipal.mekProgramada.Mask)
        frmPrincipal.mekProgramada.BackColor = &HFFFFFF
        
        frmPrincipal.pbTaskProgress.Visible = False
        frmPrincipal.tmrTaskTimer.Enabled = False
        
        frmSenha.Hide
        frmPrincipal.Show
    Else
        MsgBox ("SENHA INCORRETA!"), vbExclamation, "��� ATEN��O ���"
    End If
End Sub

Private Sub imgSobre_Click()
    frmSobre.Show
End Sub
