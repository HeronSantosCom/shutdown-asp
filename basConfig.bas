Attribute VB_Name = "basConfig"
'==================================================
'          Carrega SysIcon
'==================================================
Public Function abreSysIcon()
    If Right(App.Path, 1) = "\" Then
        frmHide.SysIcon.Icon = LoadPicture(App.Path & "shutdown.ico")
        frmHide.SysIcon.ToolTipText = "Shut Down by Telos Online - Desativado!"
        frmHide.SysIcon.Enabled = True
        frmHide.SysIcon.Visible = True
    Else
        frmHide.SysIcon.Icon = LoadPicture(App.Path & "\shutdown.ico")
        frmHide.SysIcon.ToolTipText = "Shut Down by Telos Online - Desativado!"
        frmHide.SysIcon.Enabled = True
        frmHide.SysIcon.Visible = True
    End If
End Function

'==================================================
'          Carrega as peles
'==================================================
Public Function abreSkin()
    ' carrega tempor�rios
    frmHide.lbltmpSkin.Caption = ""
    frmHide.lbltmpSkin.Caption = frmHide.lstskin.List(0)
    frmHide.lbltmpSkinFont.Caption = ""
    frmHide.lbltmpSkinFont.Caption = frmHide.lstskin.List(1)
    frmHide.lbltmpDespStat.Caption = ""
    frmHide.lbltmpDespStat.Caption = frmHide.lstDespertador.List(0)
    frmHide.lbltmpDespArq.Caption = ""
    frmHide.lbltmpDespArq.Caption = frmHide.lstDespertador.List(1)
    frmHide.lbltmpStart.Caption = ""
    frmHide.lbltmpStart.Caption = frmHide.lstHide.List(1)
    frmHide.lbltmpWinIcone.Caption = ""
    frmHide.lbltmpWinIcone.Caption = frmHide.lstHide.List(0)
    frmHide.lbltmpSenha.Caption = ""
    frmHide.lbltmpSenha.Caption = frmHide.lstSenha.List(0)
    
    ' carrega skin
    If Right(App.Path, 1) = "\" Then
        strSkin = App.Path & "peles\" & frmHide.lstskin.List(0)
    Else
        strSkin = App.Path & "\peles\" & frmHide.lstskin.List(0)
    End If
    frmPrincipal.Picture = LoadPicture(strSkin)

    ' carrega cor font
    frmPrincipal.lblAtual.ForeColor = frmHide.lstskin.List(1)
    frmPrincipal.lblProgramada.ForeColor = frmHide.lstskin.List(1)
End Function

'==================================================
'          Altera o cursor para normal
'==================================================
Public Function curNormal()
    If Right(App.Path, 1) = "\" Then
        frmPrincipal.MouseIcon = LoadPicture(App.Path & "cursor.cur")
        frmPrincipal.MousePointer = 99
    Else
        frmPrincipal.MouseIcon = LoadPicture(App.Path & "\cursor.cur")
        frmPrincipal.MousePointer = 99
    End If
End Function

'==================================================
'          Altera o cursor para ocupado
'==================================================
Public Function curOcup()
    If Right(App.Path, 1) = "\" Then
        frmPrincipal.MouseIcon = LoadPicture(App.Path & "ocupado.cur")
        frmPrincipal.MousePointer = 99
    Else
        frmPrincipal.MouseIcon = LoadPicture(App.Path & "\ocupado.cur")
        frmPrincipal.MousePointer = 99
    End If
End Function

'==================================================
'          Bot�o Minimizar
'==================================================
Public Function btnMinimizar()
    frmPrincipal.Hide
End Function

'==================================================
'          Bot�o Fechar Programa
'==================================================
Public Function btnSair()
    frmHide.SysIcon.Enabled = False
    End
End Function

'==================================================
'          Habilitar SysIcon Flash
'==================================================
Public Function SysIconFlash()
    If Right(App.Path, 1) = "\" Then
        frmHide.SysIcon.FlashIcon = LoadPicture(App.Path & "shutdown2.ico")
        frmHide.SysIcon.ToolTipText = "Shut Down by Telos Online - Ativado!"
        frmHide.SysIcon.FlashInterval = 1000
        frmHide.SysIcon.FlashEnabled = True
    Else
        frmHide.SysIcon.FlashIcon = LoadPicture(App.Path & "\shutdown2.ico")
        frmHide.SysIcon.ToolTipText = "Shut Down by Telos Online - Ativado!"
        frmHide.SysIcon.FlashInterval = 1000
        frmHide.SysIcon.FlashEnabled = True
    End If
End Function

'==================================================
'          Desabilitar SysIcon Flash
'==================================================
Public Function NoSysIconFlash()
    frmHide.SysIcon.ToolTipText = "Shut Down by Telos Online - Desativado!"
    frmHide.SysIcon.FlashEnabled = False
End Function
