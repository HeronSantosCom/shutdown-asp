VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{C932BA88-4374-101B-A56C-00AA003668DC}#1.1#0"; "MSMASK32.OCX"
Begin VB.Form frmPrincipal 
   BorderStyle     =   0  'None
   Caption         =   "Shut Down by Telos Online"
   ClientHeight    =   2490
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   3750
   BeginProperty Font 
      Name            =   "Courier New"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmPrincipal.frx":0000
   LinkTopic       =   "Form1"
   MouseIcon       =   "frmPrincipal.frx":058A
   MousePointer    =   99  'Custom
   ScaleHeight     =   166
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   250
   StartUpPosition =   2  'CenterScreen
   Begin VB.Timer tmrDesligar 
      Interval        =   1
      Left            =   2520
      Top             =   120
   End
   Begin VB.Timer tmrReiniciar 
      Enabled         =   0   'False
      Interval        =   1
      Left            =   2160
      Top             =   120
   End
   Begin VB.Timer tmrHibernar 
      Enabled         =   0   'False
      Interval        =   1
      Left            =   1800
      Top             =   120
   End
   Begin VB.Timer tmrLogoff 
      Enabled         =   0   'False
      Interval        =   1
      Left            =   1440
      Top             =   120
   End
   Begin VB.Timer tmrDespertador 
      Enabled         =   0   'False
      Interval        =   1
      Left            =   1080
      Top             =   120
   End
   Begin VB.Timer tmrTaskTimer 
      Interval        =   1
      Left            =   600
      Top             =   120
   End
   Begin MSComctlLib.ProgressBar pbTaskProgress 
      Height          =   255
      Left            =   0
      TabIndex        =   4
      Top             =   2280
      Width           =   3735
      _ExtentX        =   6588
      _ExtentY        =   450
      _Version        =   393216
      BorderStyle     =   1
      Appearance      =   0
      Min             =   1e-4
      Scrolling       =   1
   End
   Begin MSMask.MaskEdBox mekProgramada 
      Height          =   225
      Left            =   120
      TabIndex        =   3
      ToolTipText     =   "Hora Programada"
      Top             =   1920
      Width           =   900
      _ExtentX        =   1588
      _ExtentY        =   397
      _Version        =   393216
      MousePointer    =   99
      Appearance      =   0
      BackColor       =   16777215
      AllowPrompt     =   -1  'True
      MaxLength       =   8
      MouseIcon       =   "frmPrincipal.frx":06DC
      Mask            =   "##:##:##"
      PromptChar      =   "0"
   End
   Begin VB.Timer tmrRelogio 
      Interval        =   1
      Left            =   120
      Top             =   120
   End
   Begin VB.Image imgSobre 
      Height          =   645
      Left            =   120
      MouseIcon       =   "frmPrincipal.frx":083E
      MousePointer    =   99  'Custom
      ToolTipText     =   "Sobre..."
      Top             =   120
      Width           =   2820
   End
   Begin VB.Image imgReiniciar 
      Height          =   270
      Left            =   2400
      MouseIcon       =   "frmPrincipal.frx":0990
      MousePointer    =   99  'Custom
      Picture         =   "frmPrincipal.frx":0AE2
      ToolTipText     =   "Reiniciar"
      Top             =   1800
      Width           =   270
   End
   Begin VB.Image imgLogoff 
      Height          =   270
      Left            =   2880
      MouseIcon       =   "frmPrincipal.frx":0DB8
      MousePointer    =   99  'Custom
      Picture         =   "frmPrincipal.frx":0F0A
      ToolTipText     =   "Logoff"
      Top             =   1320
      Width           =   270
   End
   Begin VB.Image imgHibernar 
      Enabled         =   0   'False
      Height          =   270
      Left            =   3000
      MouseIcon       =   "frmPrincipal.frx":11E8
      MousePointer    =   99  'Custom
      Picture         =   "frmPrincipal.frx":133A
      ToolTipText     =   "Hibernar"
      Top             =   480
      Visible         =   0   'False
      Width           =   270
   End
   Begin VB.Image imgDespertador 
      Height          =   270
      Left            =   2400
      MouseIcon       =   "frmPrincipal.frx":1610
      MousePointer    =   99  'Custom
      Picture         =   "frmPrincipal.frx":1762
      ToolTipText     =   "Despertador"
      Top             =   1320
      Width           =   270
   End
   Begin VB.Image imgDesligar 
      Height          =   270
      Left            =   2880
      MouseIcon       =   "frmPrincipal.frx":189B
      MousePointer    =   99  'Custom
      Picture         =   "frmPrincipal.frx":19ED
      ToolTipText     =   "Desligar"
      Top             =   1800
      Width           =   270
   End
   Begin VB.Label lblProgramada 
      BackStyle       =   0  'Transparent
      Caption         =   "Hora programada:"
      Height          =   255
      Left            =   120
      TabIndex        =   2
      Top             =   1680
      Width           =   1695
   End
   Begin VB.Label lblHora 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H80000008&
      Height          =   225
      Left            =   120
      TabIndex        =   1
      ToolTipText     =   "Hora Atual"
      Top             =   1440
      Width           =   900
   End
   Begin VB.Label lblAtual 
      BackStyle       =   0  'Transparent
      Caption         =   "Hora atual:"
      Height          =   255
      Left            =   120
      TabIndex        =   0
      Top             =   1200
      Width           =   1215
   End
   Begin VB.Image btnFechar1 
      Height          =   255
      Left            =   1920
      MouseIcon       =   "frmPrincipal.frx":1CC3
      MousePointer    =   99  'Custom
      Picture         =   "frmPrincipal.frx":1E15
      ToolTipText     =   "Sair"
      Top             =   840
      Width           =   1545
   End
   Begin VB.Image btnConfig 
      Height          =   255
      Left            =   120
      MouseIcon       =   "frmPrincipal.frx":1ED6
      MousePointer    =   99  'Custom
      Picture         =   "frmPrincipal.frx":2028
      ToolTipText     =   "Ajustar configura��es"
      Top             =   840
      Width           =   1545
   End
   Begin VB.Image btnFechar 
      Height          =   270
      Left            =   3360
      MouseIcon       =   "frmPrincipal.frx":2118
      MousePointer    =   99  'Custom
      Picture         =   "frmPrincipal.frx":226A
      ToolTipText     =   "Fechar"
      Top             =   120
      Width           =   270
   End
   Begin VB.Image btnMin 
      Height          =   270
      Left            =   3000
      MouseIcon       =   "frmPrincipal.frx":22EC
      MousePointer    =   99  'Custom
      Picture         =   "frmPrincipal.frx":243E
      ToolTipText     =   "Minimizar"
      Top             =   120
      Width           =   270
   End
End
Attribute VB_Name = "frmPrincipal"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'==================================================
'          Bot�o Configura��es
'==================================================
Private Sub btnConfig_Click()
    frmConfig.Show
End Sub

'==================================================
'          Bot�o Fechar
'==================================================
Private Sub btnFechar_Click()
    btnSair
End Sub

'==================================================
'          Bot�o Sair
'==================================================
Private Sub btnFechar1_Click()
    btnSair
End Sub

'==================================================
'          Bot�o Minimizar
'==================================================
Private Sub btnMin_Click()
    If frmHide.lbltmpWinIcone.Caption = "ON" Then
        frmSystray.Show
    Else
        btnMinimizar
    End If
End Sub

'==================================================
'          FORM LOAD
'==================================================
Private Sub Form_Load()
    pbTaskProgress.Visible = False
    pbTaskProgress.Align = vbAlignBottom
    pbTaskProgress.Min = 0
    pbTaskProgress.Max = 100

    tmrTaskTimer.Interval = 1000
    tmrTaskTimer.Enabled = False

    mekProgramada.SelStart = 0
    mekProgramada.SelLength = Len(mekProgramada.Mask)
End Sub

'==================================================
'          Bot�o Desligar
'==================================================
Private Sub imgDesligar_Click()
If Mid(mekProgramada.Text, 7, 2) > "60" Then
    MsgBox ("Segundo Programado Incorreto. Verificar!"), vbExclamation, "��� ATEN��O ���"
    mekProgramada.SelStart = 6
    mekProgramada.SelLength = 2
ElseIf Mid(mekProgramada.Text, 4, 2) > "60" Then
    MsgBox ("Minuto Programado Incorreto. Verificar!"), vbExclamation, "��� ATEN��O ���"
    mekProgramada.SelStart = 3
    mekProgramada.SelLength = 2
ElseIf Mid(mekProgramada.Text, 1, 2) > "24" Then
    MsgBox ("Hora Programada Incorreta. Verificar!"), vbExclamation, "��� ATEN��O ���"
    mekProgramada.SelStart = 0
    mekProgramada.SelLength = 2
ElseIf mekProgramada.Text <= Format(Time, "hh:mm:ss") Then
    MsgBox ("Hor�rio estipulado menor que a atual!"), vbExclamation, "Aten��o!"
    mekProgramada.SelStart = 0
    mekProgramada.SelLength = Len(mekProgramada.Mask)
Else
    If frmHide.cbxDesligar.Value = vbUnchecked Then
        frmHide.cbxDesligar.Value = vbChecked
        If Right(App.Path, 1) = "\" Then
            imgDesligar.Picture = LoadPicture(App.Path & "Desligar1.gif")
        Else
            imgDesligar.Picture = LoadPicture(App.Path & "\Desligar1.gif")
        End If
        SysIconFlash
        imgDespertador.Enabled = False
        imgLogoff.Enabled = False
        'imgHibernar.Enabled = False
        imgReiniciar.Enabled = False
        mekProgramada.BackColor = &HFFC0C0

        Atual = Format(Time, "hh:mm:ss")
        hAtual = Mid(Atual, 1, 2)
        mAtual = Mid(Atual, 4, 2)
        sAtual = Mid(Atual, 7, 2)
        strTotsA = (hAtual * 3600) + (mAtual * 60) + sAtual
        hProg = Mid(mekProgramada.Text, 1, 2)
        mProg = Mid(mekProgramada.Text, 4, 2)
        sProg = Mid(mekProgramada.Text, 7, 2)
        strTotsS = (hProg * 3600) + (mProg * 60) + sProg
        strTots = strTotsS - strTotsA
        pbTaskProgress.Value = pbTaskProgress.Min
        pbTaskProgress.Max = strTots
        
        pbTaskProgress.Visible = True
        tmrTaskTimer.Enabled = True
        tmrDesligar.Enabled = True
    Else
        frmSenha.Show
    End If
End If
End Sub

'==================================================
'          Bot�o Despertador
'==================================================
Private Sub imgDespertador_Click()
If Mid(mekProgramada.Text, 7, 2) > "60" Then
    MsgBox ("Segundo Programado Incorreto. Verificar!"), vbExclamation, "��� ATEN��O ���"
    mekProgramada.SelStart = 6
    mekProgramada.SelLength = 2
ElseIf Mid(mekProgramada.Text, 4, 2) > "60" Then
    MsgBox ("Minuto Programado Incorreto. Verificar!"), vbExclamation, "��� ATEN��O ���"
    mekProgramada.SelStart = 3
    mekProgramada.SelLength = 2
ElseIf Mid(mekProgramada.Text, 1, 2) > "24" Then
    MsgBox ("Hora Programada Incorreta. Verificar!"), vbExclamation, "��� ATEN��O ���"
    mekProgramada.SelStart = 0
    mekProgramada.SelLength = 2
ElseIf mekProgramada.Text <= Format(Time, "hh:mm:ss") Then
    MsgBox ("Hor�rio estipulado menor que a atual!"), vbExclamation, "Aten��o!"
    mekProgramada.SelStart = 0
    mekProgramada.SelLength = Len(mekProgramada.Mask)
Else
    If frmHide.cbxDespertador.Value = vbUnchecked Then
        frmHide.cbxDespertador.Value = vbChecked
        If Right(App.Path, 1) = "\" Then
            imgDespertador.Picture = LoadPicture(App.Path & "despertador1.gif")
        Else
            imgDespertador.Picture = LoadPicture(App.Path & "\despertador1.gif")
        End If
        SysIconFlash
        imgLogoff.Enabled = False
        'imgHibernar.Enabled = False
        imgReiniciar.Enabled = False
        imgDesligar.Enabled = False
        mekProgramada.BackColor = &HFFC0C0

        Atual = Format(Time, "hh:mm:ss")
        hAtual = Mid(Atual, 1, 2)
        mAtual = Mid(Atual, 4, 2)
        sAtual = Mid(Atual, 7, 2)
        strTotsA = (hAtual * 3600) + (mAtual * 60) + sAtual
        hProg = Mid(mekProgramada.Text, 1, 2)
        mProg = Mid(mekProgramada.Text, 4, 2)
        sProg = Mid(mekProgramada.Text, 7, 2)
        strTotsS = (hProg * 3600) + (mProg * 60) + sProg
        strTots = strTotsS - strTotsA
        pbTaskProgress.Value = pbTaskProgress.Min
        pbTaskProgress.Max = strTots
        
        pbTaskProgress.Visible = True
        tmrTaskTimer.Enabled = True
        tmrDespertador.Enabled = True
    Else
        frmSenha.Show
    End If
End If
End Sub

'==================================================
'          Bot�o Hibernar
'==================================================
Private Sub imgHibernar_Click()
If mekProgramada.Text <= Format(Time, "hh:mm:ss") Then
    MsgBox ("Hor�rio estipulado menor que a atual!"), vbExclamation, "Aten��o!"
    mekProgramada.SelStart = 0
    mekProgramada.SelLength = Len(mekProgramada.Mask)
Else
    If frmHide.cbxHibernar.Value = vbUnchecked Then
        frmHide.cbxHibernar.Value = vbChecked
        If Right(App.Path, 1) = "\" Then
            imgHibernar.Picture = LoadPicture(App.Path & "Hibernar1.gif")
        Else
            imgHibernar.Picture = LoadPicture(App.Path & "\Hibernar1.gif")
        End If
        SysIconFlash
        imgDespertador.Enabled = False
        imgLogoff.Enabled = False
        imgReiniciar.Enabled = False
        imgDesligar.Enabled = False
        mekProgramada.BackColor = &HFFC0C0

        Atual = Format(Time, "hh:mm:ss")
        hAtual = Mid(Atual, 1, 2)
        mAtual = Mid(Atual, 4, 2)
        sAtual = Mid(Atual, 7, 2)
        strTotsA = (hAtual * 3600) + (mAtual * 60) + sAtual
        hProg = Mid(mekProgramada.Text, 1, 2)
        mProg = Mid(mekProgramada.Text, 4, 2)
        sProg = Mid(mekProgramada.Text, 7, 2)
        strTotsS = (hProg * 3600) + (mProg * 60) + sProg
        strTots = strTotsS - strTotsA
        pbTaskProgress.Value = pbTaskProgress.Min
        pbTaskProgress.Max = strTots
        
        pbTaskProgress.Visible = True
        tmrTaskTimer.Enabled = True
        tmrHibernar.Enabled = True
    Else
        frmSenha.Show
    End If
End If
End Sub

'==================================================
'          Bot�o Logoff
'==================================================
Private Sub imgLogoff_Click()
If Mid(mekProgramada.Text, 7, 2) > "60" Then
    MsgBox ("Segundo Programado Incorreto. Verificar!"), vbExclamation, "��� ATEN��O ���"
    mekProgramada.SelStart = 6
    mekProgramada.SelLength = 2
ElseIf Mid(mekProgramada.Text, 4, 2) > "60" Then
    MsgBox ("Minuto Programado Incorreto. Verificar!"), vbExclamation, "��� ATEN��O ���"
    mekProgramada.SelStart = 3
    mekProgramada.SelLength = 2
ElseIf Mid(mekProgramada.Text, 1, 2) > "24" Then
    MsgBox ("Hora Programada Incorreta. Verificar!"), vbExclamation, "��� ATEN��O ���"
    mekProgramada.SelStart = 0
    mekProgramada.SelLength = 2
ElseIf mekProgramada.Text <= Format(Time, "hh:mm:ss") Then
    MsgBox ("Hor�rio estipulado menor que a atual!"), vbExclamation, "Aten��o!"
    mekProgramada.SelStart = 0
    mekProgramada.SelLength = Len(mekProgramada.Mask)
Else
    If frmHide.cbxLogoff.Value = vbUnchecked Then
        frmHide.cbxLogoff.Value = vbChecked
        If Right(App.Path, 1) = "\" Then
            imgLogoff.Picture = LoadPicture(App.Path & "Logoff1.gif")
        Else
            imgLogoff.Picture = LoadPicture(App.Path & "\Logoff1.gif")
        End If
        SysIconFlash
        imgDespertador.Enabled = False
        'imgHibernar.Enabled = False
        imgReiniciar.Enabled = False
        imgDesligar.Enabled = False
        mekProgramada.BackColor = &HFFC0C0

        Atual = Format(Time, "hh:mm:ss")
        hAtual = Mid(Atual, 1, 2)
        mAtual = Mid(Atual, 4, 2)
        sAtual = Mid(Atual, 7, 2)
        strTotsA = (hAtual * 3600) + (mAtual * 60) + sAtual
        hProg = Mid(mekProgramada.Text, 1, 2)
        mProg = Mid(mekProgramada.Text, 4, 2)
        sProg = Mid(mekProgramada.Text, 7, 2)
        strTotsS = (hProg * 3600) + (mProg * 60) + sProg
        strTots = strTotsS - strTotsA
        pbTaskProgress.Value = pbTaskProgress.Min
        pbTaskProgress.Max = strTots
        
        pbTaskProgress.Visible = True
        tmrTaskTimer.Enabled = True
        tmrLogoff.Enabled = True
    Else
        frmSenha.Show
    End If
End If
End Sub

'==================================================
'          Bot�o Reiniciar
'==================================================
Private Sub imgReiniciar_Click()
If Mid(mekProgramada.Text, 7, 2) > "60" Then
    MsgBox ("Segundo Programado Incorreto. Verificar!"), vbExclamation, "��� ATEN��O ���"
    mekProgramada.SelStart = 6
    mekProgramada.SelLength = 2
ElseIf Mid(mekProgramada.Text, 4, 2) > "60" Then
    MsgBox ("Minuto Programado Incorreto. Verificar!"), vbExclamation, "��� ATEN��O ���"
    mekProgramada.SelStart = 3
    mekProgramada.SelLength = 2
ElseIf Mid(mekProgramada.Text, 1, 2) > "24" Then
    MsgBox ("Hora Programada Incorreta. Verificar!"), vbExclamation, "��� ATEN��O ���"
    mekProgramada.SelStart = 0
    mekProgramada.SelLength = 2
ElseIf mekProgramada.Text <= Format(Time, "hh:mm:ss") Then
    MsgBox ("Hor�rio estipulado menor que a atual!"), vbExclamation, "Aten��o!"
    mekProgramada.SelStart = 0
    mekProgramada.SelLength = Len(mekProgramada.Mask)
Else
    If frmHide.cbxReiniciar.Value = vbUnchecked Then
        frmHide.cbxReiniciar.Value = vbChecked
        If Right(App.Path, 1) = "\" Then
            imgReiniciar.Picture = LoadPicture(App.Path & "Reiniciar1.gif")
        Else
            imgReiniciar.Picture = LoadPicture(App.Path & "\Reiniciar1.gif")
        End If
        SysIconFlash
        imgDespertador.Enabled = False
        imgLogoff.Enabled = False
        'imgHibernar.Enabled = False
        imgDesligar.Enabled = False
        mekProgramada.BackColor = &HFFC0C0

        Atual = Format(Time, "hh:mm:ss")
        hAtual = Mid(Atual, 1, 2)
        mAtual = Mid(Atual, 4, 2)
        sAtual = Mid(Atual, 7, 2)
        strTotsA = (hAtual * 3600) + (mAtual * 60) + sAtual
        hProg = Mid(mekProgramada.Text, 1, 2)
        mProg = Mid(mekProgramada.Text, 4, 2)
        sProg = Mid(mekProgramada.Text, 7, 2)
        strTotsS = (hProg * 3600) + (mProg * 60) + sProg
        strTots = strTotsS - strTotsA
        pbTaskProgress.Value = pbTaskProgress.Min
        pbTaskProgress.Max = strTots
        
        pbTaskProgress.Visible = True
        tmrTaskTimer.Enabled = True
        tmrReiniciar.Enabled = True
    Else
        frmSenha.Show
    End If
End If
End Sub

Private Sub imgSobre_Click()
    frmSobre.Show
End Sub

'==================================================
'          Timer Desligar
'==================================================
Private Sub tmrDesligar_Timer()
    If frmHide.cbxDesligar.Value = vbChecked Then
        If mekProgramada.Text = Format(Time, "hh:mm:ss") Then
            pbTaskProgress.Value = strTots
            frmHide.cbxDesligar.Value = vbUnchecked
            If Right(App.Path, 1) = "\" Then
                imgDesligar.Picture = LoadPicture(App.Path & "Desligar.gif")
            Else
                imgDesligar.Picture = LoadPicture(App.Path & "\Desligar.gif")
            End If
            NoSysIconFlash
            imgDespertador.Enabled = True
            imgLogoff.Enabled = True
            'imgHibernar.Enabled = True
            imgReiniciar.Enabled = True
            mekProgramada.SelStart = 0
            mekProgramada.SelLength = Len(mekProgramada.Mask)
            mekProgramada.BackColor = &HFFFFFF
            pbTaskProgress.Visible = False
            tmrTaskTimer.Enabled = False
            tmrDesligar.Enabled = False
            'Executa o Comando
            Variav = ExitWindowsEx(2, 0)
            cmdDesligar
            'MsgBox "DESLIGAR"
        End If
    End If
End Sub

'==================================================
'          Timer do Despertador
'==================================================
Private Sub tmrDespertador_Timer()
    If frmHide.cbxDespertador.Value = vbChecked Then
        If mekProgramada.Text = Format(Time, "hh:mm:ss") Then
            pbTaskProgress.Value = strTots
            frmHide.cbxDespertador.Value = vbUnchecked
            If Right(App.Path, 1) = "\" Then
                imgDespertador.Picture = LoadPicture(App.Path & "despertador.gif")
            Else
                imgDespertador.Picture = LoadPicture(App.Path & "\despertador.gif")
            End If
            NoSysIconFlash
            imgLogoff.Enabled = True
            'imgHibernar.Enabled = True
            imgReiniciar.Enabled = True
            imgDesligar.Enabled = True
            mekProgramada.SelStart = 0
            mekProgramada.SelLength = Len(mekProgramada.Mask)
            mekProgramada.BackColor = &HFFFFFF
            pbTaskProgress.Visible = False
            tmrTaskTimer.Enabled = False
            tmrDespertador.Enabled = False
            'Executa o Comando
                frmDespertar.Show
        End If
    End If
End Sub

'==================================================
'          Timer Hibernar
'==================================================
Private Sub tmrHibernar_Timer()
    If frmHide.cbxHibernar.Value = vbChecked Then
        If mekProgramada.Text = Format(Time, "hh:mm:ss") Then
            pbTaskProgress.Value = strTots
            frmHide.cbxHibernar.Value = vbUnchecked
            If Right(App.Path, 1) = "\" Then
                imgHibernar.Picture = LoadPicture(App.Path & "Hibernar.gif")
            Else
                imgHibernar.Picture = LoadPicture(App.Path & "\Hibernar.gif")
            End If
            NoSysIconFlash
            imgDespertador.Enabled = True
            imgLogoff.Enabled = True
            imgReiniciar.Enabled = True
            imgDesligar.Enabled = True
            mekProgramada.SelStart = 0
            mekProgramada.SelLength = Len(mekProgramada.Mask)
            mekProgramada.BackColor = &HFFFFFF
            pbTaskProgress.Visible = False
            tmrTaskTimer.Enabled = False
            tmrHibernar.Enabled = False
            'Executa o Comando
            MsgBox "HIBERNAR"
        End If
    End If
End Sub

'==================================================
'          Timer Logoff
'==================================================
Private Sub tmrLogoff_Timer()
    If frmHide.cbxLogoff.Value = vbChecked Then
        If mekProgramada.Text = Format(Time, "hh:mm:ss") Then
            pbTaskProgress.Value = strTots
            frmHide.cbxLogoff.Value = vbUnchecked
            If Right(App.Path, 1) = "\" Then
                imgLogoff.Picture = LoadPicture(App.Path & "Logoff.gif")
            Else
                imgLogoff.Picture = LoadPicture(App.Path & "\Logoff.gif")
            End If
            NoSysIconFlash
            imgDespertador.Enabled = True
            'imgHibernar.Enabled = True
            imgReiniciar.Enabled = True
            imgDesligar.Enabled = True
            mekProgramada.SelStart = 0
            mekProgramada.SelLength = Len(mekProgramada.Mask)
            mekProgramada.BackColor = &HFFFFFF
            pbTaskProgress.Visible = False
            tmrTaskTimer.Enabled = False
            tmrLogoff.Enabled = False
            'Executa o Comando
            Variav = ExitWindowsEx(EWX_LOGOFF, 0)
            LogOff
            'MsgBox "LOGOFF"
        End If
    End If
End Sub

'==================================================
'          Timer Reiniciar
'==================================================
Private Sub tmrReiniciar_Timer()
    If frmHide.cbxReiniciar.Value = vbChecked Then
        If mekProgramada.Text = Format(Time, "hh:mm:ss") Then
            pbTaskProgress.Value = strTots
            frmHide.cbxReiniciar.Value = vbUnchecked
            If Right(App.Path, 1) = "\" Then
                imgReiniciar.Picture = LoadPicture(App.Path & "Reiniciar.gif")
            Else
                imgReiniciar.Picture = LoadPicture(App.Path & "\Reiniciar.gif")
            End If
            NoSysIconFlash
            imgDespertador.Enabled = True
            imgLogoff.Enabled = True
            'imgHibernar.Enabled = True
            imgDesligar.Enabled = True
            mekProgramada.SelStart = 0
            mekProgramada.SelLength = Len(mekProgramada.Mask)
            mekProgramada.BackColor = &HFFFFFF
            pbTaskProgress.Visible = False
            tmrTaskTimer.Enabled = False
            tmrReiniciar.Enabled = False
            'Executa o Comando
            Variav = ExitWindowsEx(EWX_REBOOT, 2)
            ReBooT
            'MsgBox "REINICIAR"
        End If
    End If
End Sub

'==================================================
'          Timer Rel�gio
'==================================================
Private Sub tmrRelogio_Timer()
    lblHora.Caption = Format(Time, "hh:mm:ss")
End Sub

'==================================================
'          Timer Barra de Progresso
'==================================================
Private Sub tmrTaskTimer_Timer()
    pbTaskProgress.Value = _
        pbTaskProgress.Value + 1

    If pbTaskProgress.Value >= pbTaskProgress.Max _
    Then
        pbTaskProgress.Visible = False
        tmrTaskTimer.Enabled = False
        curNormal
    End If
End Sub
