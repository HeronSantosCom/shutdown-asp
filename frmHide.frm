VERSION 5.00
Object = "{18D91AD0-D0BE-11D1-A6B4-00AA002075DA}#1.0#0"; "FlshTray.ocx"
Begin VB.Form frmHide 
   Appearance      =   0  'Flat
   BackColor       =   &H80000005&
   BorderStyle     =   0  'None
   Caption         =   "HideWin"
   ClientHeight    =   3660
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   2940
   Icon            =   "frmHide.frx":0000
   LinkMode        =   1  'Source
   LinkTopic       =   "frmHide"
   ScaleHeight     =   3660
   ScaleWidth      =   2940
   ShowInTaskbar   =   0   'False
   Visible         =   0   'False
   WindowState     =   1  'Minimized
   Begin VB.ListBox lstSenha 
      Height          =   255
      Left            =   120
      TabIndex        =   14
      Top             =   1800
      Width           =   1215
   End
   Begin VB.CheckBox cbxReiniciar 
      Caption         =   "cbxReiniciar"
      Height          =   255
      Left            =   1440
      Style           =   1  'Graphical
      TabIndex        =   8
      Top             =   1080
      Width           =   1335
   End
   Begin VB.CheckBox cbxLogoff 
      Caption         =   "cbxLogoff"
      Height          =   255
      Left            =   1440
      Style           =   1  'Graphical
      TabIndex        =   7
      Top             =   840
      Width           =   1335
   End
   Begin VB.CheckBox cbxHibernar 
      Caption         =   "cbxHibernar"
      Enabled         =   0   'False
      Height          =   255
      Left            =   1440
      Style           =   1  'Graphical
      TabIndex        =   6
      Top             =   600
      Visible         =   0   'False
      Width           =   1335
   End
   Begin VB.CheckBox cbxDespertador 
      Caption         =   "cbxDespertador"
      Height          =   255
      Left            =   1440
      Style           =   1  'Graphical
      TabIndex        =   5
      Top             =   360
      Width           =   1335
   End
   Begin TrayIconPrj.TrayIcon SysIcon 
      Left            =   120
      Top             =   120
      _ExtentX        =   1905
      _ExtentY        =   953
      Icon            =   "frmHide.frx":058A
      ToolTipText     =   ""
      Enabled         =   0   'False
      TrueClick       =   0   'False
      Visible         =   0   'False
      FlashSound      =   0
      FlashIcon       =   "frmHide.frx":08A4
      FlashInterval   =   1000
      FlashEnabled    =   0   'False
   End
   Begin VB.ListBox lstskin 
      BeginProperty Font 
         Name            =   "Microsoft Sans Serif"
         Size            =   8.25
         Charset         =   238
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      ItemData        =   "frmHide.frx":0BBE
      Left            =   120
      List            =   "frmHide.frx":0BC5
      TabIndex        =   3
      Top             =   720
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.CheckBox cbxDesligar 
      Caption         =   "cbxDesligar"
      Height          =   255
      Left            =   1440
      Style           =   1  'Graphical
      TabIndex        =   2
      Top             =   120
      Width           =   1335
   End
   Begin VB.ListBox lstHide 
      Height          =   255
      Left            =   120
      TabIndex        =   1
      Top             =   1080
      Width           =   1215
   End
   Begin VB.ListBox lstDespertador 
      Height          =   255
      Left            =   120
      TabIndex        =   0
      Top             =   1440
      Width           =   1215
   End
   Begin VB.Label lbltmpWinIcone 
      Caption         =   "lbltmpWinIcone"
      Height          =   255
      Left            =   1440
      TabIndex        =   16
      Top             =   2880
      Width           =   1215
   End
   Begin VB.Label lbltmpStart 
      Caption         =   "lbltmpStart"
      Height          =   255
      Left            =   120
      TabIndex        =   15
      Top             =   2880
      Width           =   1215
   End
   Begin VB.Label lbltmpSenha 
      Caption         =   "lbltmpSenha"
      Height          =   255
      Left            =   120
      TabIndex        =   13
      Top             =   3240
      Width           =   1215
   End
   Begin VB.Label lbltmpAcao 
      Caption         =   "lbltmpAcao"
      Height          =   255
      Left            =   1440
      TabIndex        =   12
      Top             =   1440
      Width           =   1335
   End
   Begin VB.Label lbltmpDespStat 
      Caption         =   "lbltmpDespStat"
      Height          =   255
      Left            =   1440
      TabIndex        =   11
      Top             =   2520
      Width           =   1215
   End
   Begin VB.Label lbltmpDespArq 
      Caption         =   "lbltmpDespArq"
      Height          =   255
      Left            =   120
      TabIndex        =   10
      Top             =   2520
      Width           =   1215
   End
   Begin VB.Label lbltmpSkinFont 
      Caption         =   "lbltmpSkinFont"
      Height          =   255
      Left            =   1440
      TabIndex        =   9
      Top             =   2160
      Width           =   1215
   End
   Begin VB.Label lbltmpSkin 
      Caption         =   "lbltmpSkin"
      Height          =   255
      Left            =   120
      TabIndex        =   4
      Top             =   2160
      Width           =   1215
   End
End
Attribute VB_Name = "frmHide"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'==================================================
'          Declaração
'==================================================
Dim keynames() As String
Dim arrcount As Long
Dim nkeyname As Long
Dim skeyname As String
Dim strvalue As String

'==================================================
'          Carrega Form
'==================================================
Private Sub Form_Load()
    If Right(App.Path, 1) = "\" Then
        cfgFile = App.Path & "config.ini"
    Else
        cfgFile = App.Path & "\config.ini"
    End If
    
    arrcount = INIQuerySettingNames("Skin", keynames, , cfgFile)
    lstskin.Clear
    For nkeyname = 0 To arrcount - 1
        skeyname = keynames(nkeyname)
        INIQuerySetting "Skin", skeyname, strvalue, , cfgFile
        lstskin.AddItem strvalue
    Next
    Erase keynames
    
    arrcount = INIQuerySettingNames("Hide", keynames, , cfgFile)
    lstHide.Clear
    For nkeyname = 0 To arrcount - 1
        skeyname = keynames(nkeyname)
        INIQuerySetting "Hide", skeyname, strvalue, , cfgFile
        lstHide.AddItem strvalue
    Next
    Erase keynames
    
    arrcount = INIQuerySettingNames("Despertador", keynames, , cfgFile)
    lstDespertador.Clear
    For nkeyname = 0 To arrcount - 1
        skeyname = keynames(nkeyname)
        INIQuerySetting "Despertador", skeyname, strvalue, , cfgFile
        lstDespertador.AddItem strvalue
    Next
    Erase keynames

    arrcount = INIQuerySettingNames("Senha", keynames, , cfgFile)
    lstSenha.Clear
    For nkeyname = 0 To arrcount - 1
        skeyname = keynames(nkeyname)
        INIQuerySetting "Senha", skeyname, strvalue, , cfgFile
        lstSenha.AddItem strvalue
    Next
    Erase keynames

    abreSysIcon
    abreSkin
    curNormal
    
    ' inicia escondido
    If lbltmpStart.Caption = "ON" Then
        Me.Hide
        frmPrincipal.Hide
    Else
        Me.Hide
        frmPrincipal.Show
    End If

End Sub

'==================================================
'          Duplo-clique do SysIcon
'==================================================
Private Sub SysIcon_LeftButtonDoubleClick()
    frmPrincipal.Show
End Sub
