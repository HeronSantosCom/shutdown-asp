VERSION 5.00
Begin VB.Form frmSystray 
   Appearance      =   0  'Flat
   BackColor       =   &H80000005&
   BorderStyle     =   0  'None
   Caption         =   "Icone escondido..."
   ClientHeight    =   3735
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   4935
   BeginProperty Font 
      Name            =   "Courier New"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmSystray.frx":0000
   LinkTopic       =   "Form1"
   MouseIcon       =   "frmSystray.frx":058A
   MousePointer    =   99  'Custom
   ScaleHeight     =   3735
   ScaleWidth      =   4935
   StartUpPosition =   2  'CenterScreen
   Begin VB.Image imgOk 
      Height          =   375
      Left            =   1680
      MouseIcon       =   "frmSystray.frx":06DC
      MousePointer    =   99  'Custom
      Picture         =   "frmSystray.frx":082E
      ToolTipText     =   "Fechar esta janela"
      Top             =   3240
      Width           =   1575
   End
   Begin VB.Shape Shape3 
      BorderWidth     =   2
      DrawMode        =   1  'Blackness
      Height          =   600
      Left            =   2850
      Shape           =   2  'Oval
      Top             =   1200
      Width           =   600
   End
   Begin VB.Shape Shape2 
      BorderWidth     =   2
      DrawMode        =   1  'Blackness
      Height          =   600
      Left            =   465
      Shape           =   2  'Oval
      Top             =   1200
      Width           =   600
   End
   Begin VB.Label Label2 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      Caption         =   "Para reabr�-lo novamente, d� um duplo-clique so icone demostrado acima!"
      Height          =   375
      Left            =   300
      TabIndex        =   1
      Top             =   2640
      Width           =   4335
   End
   Begin VB.Label Label1 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      Caption         =   "Ao minimizar, o programa fica escondido na bandeja do sistema, permitindo um maior conforto nos seus afazeres."
      Height          =   615
      Left            =   300
      TabIndex        =   0
      Top             =   1920
      Width           =   4335
   End
   Begin VB.Shape Shape1 
      Height          =   3735
      Left            =   0
      Top             =   0
      Width           =   4935
   End
   Begin VB.Image Image3 
      Appearance      =   0  'Flat
      BorderStyle     =   1  'Fixed Single
      Height          =   780
      Left            =   2520
      Picture         =   "frmSystray.frx":0912
      Top             =   960
      Width           =   2280
   End
   Begin VB.Image Image2 
      Appearance      =   0  'Flat
      BorderStyle     =   1  'Fixed Single
      Height          =   780
      Left            =   120
      Picture         =   "frmSystray.frx":105E
      Top             =   960
      Width           =   2280
   End
   Begin VB.Image imgSobre 
      Height          =   645
      Left            =   120
      MouseIcon       =   "frmSystray.frx":15B0
      MousePointer    =   99  'Custom
      Picture         =   "frmSystray.frx":1702
      ToolTipText     =   "Sobre..."
      Top             =   120
      Width           =   2820
   End
End
Attribute VB_Name = "frmSystray"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub imgOk_Click()
    If Right(App.Path, 1) = "\" Then
        cfgFile = App.Path & "config.ini"
    Else
        cfgFile = App.Path & "\config.ini"
    End If
    
    INIClearSetting "Hide", "WinIcone", cfgFile
    INIWriteSetting "Hide", "WinIcone", "OFF", cfgFile

    frmHide.lbltmpWinIcone.Caption = ""
    frmHide.lbltmpWinIcone.Caption = "OFF"

    frmSystray.Hide
    btnMinimizar
End Sub

Private Sub imgSobre_Click()
    frmSobre.Show
End Sub
