VERSION 5.00
Begin VB.Form frmDespertar 
   Appearance      =   0  'Flat
   BackColor       =   &H80000005&
   BorderStyle     =   0  'None
   Caption         =   "Despertador..."
   ClientHeight    =   7200
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   9600
   BeginProperty Font 
      Name            =   "Courier New"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmDespertar.frx":0000
   LinkTopic       =   "Form1"
   MouseIcon       =   "frmDespertar.frx":058A
   MousePointer    =   99  'Custom
   ScaleHeight     =   480
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   640
   StartUpPosition =   2  'CenterScreen
   Begin VB.Timer tmrRelogio2 
      Interval        =   1
      Left            =   1320
      Top             =   720
   End
   Begin VB.Timer tmrSom 
      Enabled         =   0   'False
      Interval        =   10
      Left            =   720
      Top             =   720
   End
   Begin VB.Shape Shape1 
      Height          =   7200
      Left            =   0
      Top             =   0
      Width           =   9600
   End
   Begin VB.Label lblDia 
      Alignment       =   2  'Center
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "BOM DIA!"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   48
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1170
      Left            =   2168
      TabIndex        =   1
      Top             =   3015
      Width           =   5265
   End
   Begin VB.Image imgOk 
      Height          =   375
      Left            =   4020
      MouseIcon       =   "frmDespertar.frx":06DC
      MousePointer    =   99  'Custom
      Picture         =   "frmDespertar.frx":082E
      ToolTipText     =   "Fechar esta janela!"
      Top             =   4980
      Width           =   1575
   End
   Begin VB.Label lblHora 
      Alignment       =   2  'Center
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "00:00:00"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   18
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   435
      Left            =   3885
      TabIndex        =   0
      Top             =   2070
      Width           =   1845
   End
End
Attribute VB_Name = "frmDespertar"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'==================================================
'          Ativa o Som
'==================================================
Private Declare Function sndPlaySound Lib "winmm.dll" Alias "sndPlaySoundA" (ByVal lpszSoundName As String, ByVal uFlags As Long) As Long
Private Declare Function mciSendString Lib "winmm.dll" Alias "mciSendStringA" (ByVal lpstrCommand As String, ByVal lpstrReturnString As String, ByVal uReturnLength As Long, ByVal hwndCallback As Long) As Long

'==================================================
'          TocaSom2
'==================================================
Public Function TocaSom2()
    On Error Resume Next
    txtArqSomAS = frmHide.lbltmpDespArq.Caption
        If txtArqSomAS = "" Then
        MsgBox "Especifique um arquivo de som.", vbOKOnly + vbCritical
    Else
        sndPlaySound txtArqSomAS, 0
        i = mciSendString("open " & txtArqSomAS & " type sequencer alias myfile", 0&, 0, 0)
        i = mciSendString("play myfile wait", 0&, 0, 0)
        i = mciSendString("close myfile", 0&, 0, 0)
    End If
End Function

'==================================================
'          Form Load
'==================================================
Private Sub Form_Activate()
    strHora = Format(Time, "hh")
    If strHora >= "00" And strHora < "12" Then
        lblDia.Caption = "BOM DIA!"
    End If
    If strHora >= "12" And strHora < "18" Then
        lblDia.Caption = "BOA TARDE!"
    End If
    If strHora >= "18" And strHora < "24" Then
        lblDia.Caption = "BOA NOITE!"
    End If
    tmrRelogio2.Enabled = True
    If frmHide.lbltmpDespStat.Caption = "ON" Then
        tmrSom.Enabled = True
    End If
End Sub

'==================================================
'          Bot�o Ok
'==================================================
Private Sub imgOk_Click()
    tmrRelogio2.Enabled = False
    frmDespertar.Hide
End Sub

'==================================================
'          Timer do Rel�gio
'==================================================
Private Sub tmrRelogio2_Timer()
    lblHora.Caption = Format(Time, "hh:mm:ss")
End Sub

'==================================================
'          Timer do Som
'==================================================
Private Sub tmrSom_Timer()
If tmrSom.Enabled = True Then
    TocaSom2
    tmrSom.Enabled = False
End If
End Sub
